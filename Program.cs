﻿using System;
 
 namespace comp5002_10002002_assessment01
 {
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Hello! Welcome to my shop!"); // Welcome message
        Console.WriteLine();
        Console.WriteLine("Enter your name."); // Input name
        string name = (Console.ReadLine());
        Console.WriteLine();
        Console.WriteLine("Your name is {0}", name); //Displaying name
        Console.WriteLine();
        Console.WriteLine("Enter a number with two decimal places."); // Enter number with 2 decimal places
        double first = double.Parse(Console.ReadLine());
        Console.WriteLine();
       
       
        Start:
            Console.WriteLine("Would you like to enter another number? Type Y or N."); // Y/N if else statement for another number
            string answer = Console.ReadLine();
           
            double second = 0;
 
            if (answer == "Y")
   
                    {
                    Console.WriteLine("Enter your second number "); // If Y, enter second number 
                    second = double.Parse(Console.ReadLine());
                    }
                   
 
            else if (answer == "N")
            goto End;
 
 
            else
            {
                Console.WriteLine("Invalid answer, please try again."); // Shown if answer is anything other than Y/N
                goto Start;
            }
           
            End:
                double result = first+second;
                Console.WriteLine("Your first number was: {0}", first);
                Console.WriteLine("Your second number was: {0}", second);
                Console.WriteLine("Your result was: {0}", result);
                Console.WriteLine($"The sum of the number(s) including GST is {result * 1.15}"); // Displaying the result of numbers including GST
                Console.WriteLine();
                Console.WriteLine($"Thank you for shopping with us {name}, please come again!"); // Goodbye message

                Console.WriteLine("Press any key to exit the program."); // Enter any key to exit
                Console.ReadKey();
       
    }
}
 }
